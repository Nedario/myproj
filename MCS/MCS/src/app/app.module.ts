import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { MenuComponent } from './menu/menu.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ContentComponent } from './content/content.component';
import { LoggedComponent } from './logged/logged.component';
import { LoginComponent } from './login/login.component';
import { MenuLoginComponent } from './menu-login/menu-login.component';
import { LoginPageComponent } from './login-page/login-page.component';

const appRoutes: Routes = [
  { path: '', redirectTo: '/content', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  {
    path: 'content', component: ContentComponent, children: [
      {
        path: 'home', component: HomeComponent, children: [
          { path: 'personal', component: LoggedComponent }
        ]
      }
    ]
  },
];
  // {
  //   path: 'heroes',
  //   component: HeroListComponent,
  //   data: { title: 'Heroes List' }
  // },
  // { path: '',
  //   redirectTo: '/heroes',
  //   pathMatch: 'full'
  // },
  // { path: '**', component: PageNotFoundComponent }

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MenuComponent,
    SidebarComponent,
    ContentComponent,
    LoggedComponent,
    LoginComponent,
    MenuLoginComponent,
    LoginPageComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
